//
//  ModalViewControllerDelegate.swift
//  IOSProject1
//
//  Created by Ivan Phytsyk on 3/2/17.
//  Copyright © 2017 Ivan Phytsyk. All rights reserved.
//

import Foundation
public protocol ModalViewControllerDelegate
{
    func sendProjectData(data: ProjectData)
}
