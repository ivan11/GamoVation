//
//  ProjectData.swift
//  IOSProject1
//
//  Created by Ivan Phytsyk on 3/2/17.
//  Copyright © 2017 Ivan Phytsyk. All rights reserved.
//

import Foundation

public class ProjectData {
    private var _url: String = ""
    public var url: String {
        get {
            return self._url;
        }
        set {
            self._url = newValue;
        }
    }

    
    private var _name: String = ""
    public var name: String {
        get {
            return self._name;
        }
        set {
            self._name = newValue;
        }
    }
//    public var today: [Int] {
//        get {
//            return self.today;
//        }
//        set {
//            self.today = newValue;
//        }
//    }
//    public var yesterday: [Int] {
//        get{
//            return self.yesterday;
//        }
//        set {
//            self.yesterday = newValue;
//        }
//    }
//    public var month: [Int] {
//        get{
//            return self.month;
//        }
//        set {
//            self.month = newValue;
//        }
//    }
    public var data = [[Int]]()
    
}
