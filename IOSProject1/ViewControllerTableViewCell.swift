//
//  ViewControllerTableViewCell.swift
//  IOSProject1
//
//  Created by Ivan Phytsyk on 28.02.17.
//  Copyright © 2017 Ivan Phytsyk. All rights reserved.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var projectName: UILabel!
    @IBOutlet weak var todayEuro: UILabel!
    @IBOutlet weak var yesterdayEuro: UILabel!
    @IBOutlet weak var monthEuro: UILabel!
    @IBOutlet weak var todayPeople: UILabel!
    @IBOutlet weak var yesterdayPeople: UILabel!
    @IBOutlet weak var monthPeople: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
