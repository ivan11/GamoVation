//
//  ViewController.swift
//  IOSProject1
//
//  Created by Ivan Phytsyk on 28.02.17.
//  Copyright © 2017 Ivan Phytsyk. All rights reserved.
//

import UIKit
import AVFoundation
import CSV

class MainVC: UITableViewController, ModalViewControllerDelegate {
    
    private let STORAGE_ID = "ProjectsUrls";
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var counter1: UILabel!
    @IBOutlet weak var counter2: UILabel!
    @IBOutlet weak var counter3: UILabel!
    @IBOutlet weak var counter4: UILabel!
    @IBOutlet weak var counter5: UILabel!
    @IBOutlet weak var counter6: UILabel!
    
    var smallCash = AVAudioPlayer()
    var bigCash = AVAudioPlayer()
    
    var projectData: [ProjectData] = []
    
    var timer: Timer!;
    
    func musicInit() {
        do {
            smallCash = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "Hover", ofType: "mp3")!))
            smallCash.prepareToPlay()
            bigCash = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "Collect Cash", ofType: "mp3")!))
            bigCash.prepareToPlay()
            
        } catch {
            print(error)
        }
    }
    
    func smallCashPlay() {
        smallCash.play()
    }
    
    func bigCashPlay() {
        bigCash.play()
    }
    
    func calculateAll() -> [[Int]] {
        var calculatedArray: [[Int]]
        calculatedArray = [[0,0],[0,0],[0,0]]
        if (!projectData.isEmpty) {
            for i in 0...projectData.count - 1 {
                calculatedArray[0][0]+=projectData[i].data[0][0]
                calculatedArray[0][1]+=projectData[i].data[0][1]
                calculatedArray[1][0]+=projectData[i].data[1][0]
                calculatedArray[1][1]+=projectData[i].data[1][1]
                calculatedArray[2][0]+=projectData[i].data[2][0]
                calculatedArray[2][1]+=projectData[i].data[2][1]
                
            }
        }
        return calculatedArray
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        musicInit()
        
        self.headerView.backgroundColor = UIColor.clear
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "bg"))
        self.tableView.separatorStyle = .none
        calculateLabelValues()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive(_:)),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onApplicationBecomeNotActive),
            name: NSNotification.Name.UIApplicationDidEnterBackground,
            object: nil)
        
        timer = Timer.scheduledTimer(timeInterval: 60,
                             target: self,
                             selector: #selector(updateProjects),
                             userInfo: nil,
                             repeats: true)
        
    }
    
    @objc private func onApplicationBecomeNotActive() {
        timer.invalidate()
    }
    
    @objc private func updateProjects() {
        let previousFullAmount = calculateAll()[0][0]
        let defaults = UserDefaults.standard
        if let projects  = defaults.array(forKey: STORAGE_ID) as? [[String:String]] {
            var projectNumber = 0;
            for project in projects {
                let key = project.keys.first!
                let url = project[key]!
                RemoteRepository.getRemoteFile(url: url, name: key, callback: {(project: ProjectData) -> Void in
                    projectNumber += 1
                    
                    let index = self.projectData.index(where: {$0.url == url})
                    if index != nil {
                        self.projectData[index!] = project;
                    } else {
                        self.projectData.append(project)
                    }
                    
                    self.projectData.sort(by: {$0.data[2][0] > $1.data[2][0]})
                    
                    self.tableView.reloadData()
                    if (previousFullAmount != self.calculateAll()[0][0]) {
                        self.calculateLabelValues()
                    }
                    
                    
                    if (projectNumber == projects.count - 1) {
                        let difference = self.calculateAll()[0][0] - previousFullAmount
                        if (difference > 0) {
                            if (difference < 50) {
                                self.smallCashPlay()
                            } else{
                                self.bigCashPlay()
                            }
                        }
                    }
                }
                )
            }}
    }
    
    func applicationDidBecomeActive(_ notification: NSNotification) {
        updateProjects()
    }
    
    func saveCurrentProjects() {
        let defaults = UserDefaults.standard
        var urls = [[String: String]]()
        for data in projectData {
            urls.append([data.name: data.url])
        }
        defaults.setValue(urls, forKey: STORAGE_ID)
        defaults.synchronize()
    }
    
    private func calculateLabelValues() {
        var counterLabelArray : [UILabel]
        counterLabelArray = [counter1,counter2,counter3,counter4,counter5,counter6]
        let calc = String(calculateAll()[0][0])
        let counterArray = [Character](calc.characters)
        for i in 0...counterLabelArray.count-1 {
            counterLabelArray[i].text = ""
            
            rotate(view: counterLabelArray[i])
            
            if (counterArray.count > i) {
                counterLabelArray[i].text = String(counterArray[counterArray.count-1-i])
            }
        }
    }
    
    private func rotate(view: UILabel) {
        view.pushTransition(0.4)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return projectData.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ViewControllerTableViewCell
        if indexPath.section == 0 {
            cell.projectName.text = "All Projects"
            cell.deleteButton.isHidden = true
            
            let calc = calculateAll()
            
            cell.todayEuro.text = "€ \(calc[0][0].stringFormattedWithSeparator)"
            cell.todayPeople.text = "\(calc[0][1].stringFormattedWithSeparator)"
            cell.yesterdayEuro.text = "€ \(calc[1][0].stringFormattedWithSeparator)"
            cell.yesterdayPeople.text = "\(calc[1][1].stringFormattedWithSeparator)"
            cell.monthEuro.text = "€ \(calc[2][0].stringFormattedWithSeparator)"
            cell.monthPeople.text = "\(calc[2][1].stringFormattedWithSeparator)"
            
        } else {
            let data = projectData[indexPath.row]
            cell.deleteButton.isHidden = false
            cell.projectName.text = data.name
            cell.todayEuro.text = "€ " + String(data.data[0][0].stringFormattedWithSeparator)
            cell.todayPeople.text = String(data.data[0][1].stringFormattedWithSeparator)
            cell.yesterdayEuro.text = "€ " + String(data.data[1][0].stringFormattedWithSeparator)
            cell.yesterdayPeople.text = String(data.data[1][1].stringFormattedWithSeparator)
            cell.monthEuro.text = "€ " + String(data.data[2][0].stringFormattedWithSeparator)
            cell.monthPeople.text = String(data.data[2][1].stringFormattedWithSeparator)
            
            cell.deleteButton.tag = indexPath.row;
            cell.deleteButton.addTarget(self, action:  #selector(removeButtonClicked(sender:)), for: .touchUpInside);
        }
        
        cell.backgroundColor = UIColor.clear
        cell.projectName.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "devider"))
        cell.wrapperView.backgroundColor = UIColor.clear
        
        return cell
    }
    
    
    func removeButtonClicked(sender:UIButton)
    {
        let index = sender.tag
        self.tableView.beginUpdates()
        let indexPath = IndexPath(item: index, section: 1)
        projectData.remove(at: index)
        self.tableView.deleteRows(at: [indexPath], with: .fade)
        let headerIndex = IndexPath(item: 0, section: 0)
        self.tableView.reloadRows(at: [headerIndex], with: .fade)
        self.tableView.reloadData()
        self.tableView.endUpdates()
        calculateLabelValues()
        saveCurrentProjects()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let popupVC = segue.destination as! PopupVC
        popupVC.modalViewDelegate = self
    }
    
    func sendProjectData(data: ProjectData) {
        
        let index = self.projectData.index(where: {$0.url == data.url})
        if index != nil {
            self.projectData[index!] = data;
        } else {
            self.projectData.append(data)
            self.projectData.sort(by: {$0.data[2][0] > $1.data[2][0]})
            let newIndex = self.projectData.index(where: {$0 === data})
            self.tableView.beginUpdates()
            let indexPath = IndexPath(item: newIndex!, section: 1)
            self.tableView.insertRows(at: [indexPath], with: .fade)
            let headerIndex = IndexPath(item: 0, section: 0)
            self.tableView.reloadRows(at: [headerIndex], with: .fade)
            self.tableView.endUpdates()
        }
        calculateLabelValues()
        saveCurrentProjects()
    }
    
}

struct Number {
    static let formatterWithSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Integer {
    var stringFormattedWithSeparator: String {
        return Number.formatterWithSeparator.string(from: self as! NSNumber) ?? ""
    }
}

extension UILabel{
    func pushTransition(_ duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionPush)
    }
}
