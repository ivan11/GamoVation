//
//  PopupVC.swift
//  IOSProject1
//
//  Created by Ivan Phytsyk on 28.02.17.
//  Copyright © 2017 Ivan Phytsyk. All rights reserved.
//

import UIKit
import CSV

class PopupVC: UIViewController{
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var projectNameInput: UITextField!
    @IBOutlet weak var projectUrlInput: UITextField!
    
    public var modalViewDelegate: ModalViewControllerDelegate!
    
    @IBAction func closePopup(_ sender: Any) {
        loadTxt()
        
    }
    
    func loadTxt() {
        if (projectUrlInput.text!.isEmpty) {
            return
        }
        let url = projectUrlInput.text!
       
        let urlAddress = URL(string: url)!
        let name = self.projectNameInput.text!
        
        if (UIApplication.shared.canOpenURL(urlAddress)) {
            RemoteRepository.getRemoteFile(url: url, name: name, callback: {(project: ProjectData) -> Void in
                self.modalViewDelegate.sendProjectData(data: project)
                self.dismiss(animated: true, completion: nil)
            })
        } else {
            self.projectUrlInput.layer.borderWidth = 2
            self.projectUrlInput.layer.borderColor = UIColor.red.cgColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popupView.layer.cornerRadius = 10
        
        self.projectUrlInput.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        self.projectUrlInput.layer.borderWidth = 0
    }
    
}


