//
//  RemoteRepository.swift
//  IOSProject1
//
//  Created by Ivan Phytsyk on 3/2/17.
//  Copyright © 2017 Ivan Phytsyk. All rights reserved.
//

import Foundation
import CSV

public class RemoteRepository {
    public static func getRemoteFile(url: String, name: String, callback: @escaping (_ project: ProjectData) -> Void) {
        let urlAddress = URL(string: url)!
        let session = URLSession.shared.dataTask(with: urlAddress) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
            let statusCode = httpResponse.statusCode
            if statusCode == 200 {
                if let dataString = String(data: data!, encoding: .utf8) {
                    print(dataString)
                    DispatchQueue.main.async {
                        let projectData = ProjectData()
                        projectData.name = name
                        projectData.url = url
                       
                        let csv = try! CSV(string: dataString);
                        
                        var i: Int = 0;
                        for row in csv {
                            projectData.data.append([])
                            projectData.data[i].append(Int(row.first!)!)
                            projectData.data[i].append(Int(row.last!)!)
                            i+=1
                        }
                        callback(projectData)
                    }
                    
                } else {
                    print("file is empty")
                }
            } else {
                print("can't read this file. Status code \(statusCode)")
            }
        }
        }
        
        session.resume()
    }
}
